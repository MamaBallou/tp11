﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using editeur;

namespace TestEditeur
{
    [TestClass]
    public class EditeurTest
    {
        [TestMethod]
        public void TestLoad()
        {
            String path = @"../../../test.txt";
            string text = "Hello World";
            File.WriteAllText(path, text);

            Assert.AreEqual(Editeur.load("test.txt"), "Hello World");
            Assert.AreEqual(Editeur.load("test"), "Hello World");
            if (File.Exists(@"../../../test.txt"))
            {
                File.Delete(@"../../../test.txt");
                Assert.AreEqual(Editeur.load(@"test.txt"), null);
            }
            
        }

        [TestMethod]
        public void TestSave()
        {
            string text = "Hello World";
            Editeur.save("test.txt", text);
            Assert.AreEqual(File.ReadAllText(@"../../../test.txt"), "Hello World");
            
            string Text = "helle world";
            Editeur.save("test", Text);
            Assert.AreEqual(File.ReadAllText(@"../../../test.txt"), "helle world");
        }
    }
}
