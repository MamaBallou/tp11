﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace editeur
{
    public class Editeur
    {
        public static string load(string name)
        {
            string Text = null;
            if (name.EndsWith(".txt"))
            {
                name = string.Format("../../../{0}", name);
            }
            else
            {
                name = string.Format("../../../{0}.txt", name);
            }
            if (File.Exists(name)) Text = File.ReadAllText(name);
            return Text;
        }

        public static void save(string name, string content)
        {
            if (name.EndsWith(".txt"))
            {
                name = string.Format("../../../{0}", name);
            }
            else
            {
                name = string.Format("../../../{0}.txt", name);
            }
            File.WriteAllText(name, content);
        }
    }
}
