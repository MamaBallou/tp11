﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace tp11
{
    class Program
    {
        static void Main(string[] args)
        {
            String path = @"../../../test.txt";
            string text = "Hello World";
            List<string> lines = new List<string>();
            lines.Add("I'm you father");
            lines.Add("No... No... It's not true. That's impossible !");
            lines.Add("Search your feelings, you know it to be true");
            lines.Add("Noooooooo !!!! Noooo !!!");
            File.WriteAllText(path, text);
            File.WriteAllLines(path, lines);

            List<string> persos = new List<string>();
            persos.Add("Ben Kenobi");
            persos.Add("Luke Skywalker");
            persos.Add("Darth Vader");
            persos.Add("Darth Sidious");
            persos.Add("Han Solo");

            path = @"../../../rebels.txt";

            using (StreamWriter file = new StreamWriter(path))
            {
                foreach (string perso in persos)
                {
                    if (perso.Contains("Darth") == false) file.WriteLine(perso);
                }
            }

            using (StreamWriter file = new StreamWriter(path, true))
            {
                file.WriteLine("Léïa Organa"); // Add this line at the end of the text file
                file.WriteLine("Wedge Antilles");
            }

            path = @"../../../test.txt";

            string Text = File.ReadAllText(path);
            Console.WriteLine(Text);

            path = @"../../../rebels.txt";
            Console.WriteLine("Rebels : ");
            string[] Lines = File.ReadAllLines(path);
            foreach (string line in Lines)
            {
                Console.WriteLine(line);
            }
            Console.ReadKey();
        }
    }
}
