﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_editeurText
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txt_content = New System.Windows.Forms.TextBox()
        Me.txt_name = New System.Windows.Forms.TextBox()
        Me.lbl_name = New System.Windows.Forms.Label()
        Me.btn_save = New System.Windows.Forms.Button()
        Me.btn_charge = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txt_content
        '
        Me.txt_content.Location = New System.Drawing.Point(19, 53)
        Me.txt_content.Multiline = True
        Me.txt_content.Name = "txt_content"
        Me.txt_content.Size = New System.Drawing.Size(882, 451)
        Me.txt_content.TabIndex = 0
        '
        'txt_name
        '
        Me.txt_name.Location = New System.Drawing.Point(242, 12)
        Me.txt_name.Name = "txt_name"
        Me.txt_name.Size = New System.Drawing.Size(188, 26)
        Me.txt_name.TabIndex = 1
        '
        'lbl_name
        '
        Me.lbl_name.AutoSize = True
        Me.lbl_name.Location = New System.Drawing.Point(15, 15)
        Me.lbl_name.Name = "lbl_name"
        Me.lbl_name.Size = New System.Drawing.Size(221, 20)
        Me.lbl_name.TabIndex = 2
        Me.lbl_name.Text = "Donnez le nom du document :"
        '
        'btn_save
        '
        Me.btn_save.Location = New System.Drawing.Point(777, 9)
        Me.btn_save.Name = "btn_save"
        Me.btn_save.Size = New System.Drawing.Size(124, 32)
        Me.btn_save.TabIndex = 3
        Me.btn_save.Text = "Sauvegarder"
        Me.btn_save.UseVisualStyleBackColor = True
        '
        'btn_charge
        '
        Me.btn_charge.Location = New System.Drawing.Point(683, 9)
        Me.btn_charge.Name = "btn_charge"
        Me.btn_charge.Size = New System.Drawing.Size(88, 32)
        Me.btn_charge.TabIndex = 4
        Me.btn_charge.Text = "Charger"
        Me.btn_charge.UseVisualStyleBackColor = True
        '
        'frm_editeurText
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(913, 516)
        Me.Controls.Add(Me.btn_charge)
        Me.Controls.Add(Me.btn_save)
        Me.Controls.Add(Me.lbl_name)
        Me.Controls.Add(Me.txt_name)
        Me.Controls.Add(Me.txt_content)
        Me.Name = "frm_editeurText"
        Me.Text = "Editeur de Texte"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txt_content As TextBox
    Friend WithEvents txt_name As TextBox
    Friend WithEvents lbl_name As Label
    Friend WithEvents btn_save As Button
    Friend WithEvents btn_charge As Button
End Class
