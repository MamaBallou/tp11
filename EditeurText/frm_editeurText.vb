﻿Imports editeur

Public Class frm_editeurText
    Private Sub btn_charge_Click(sender As Object, e As EventArgs) Handles btn_charge.Click
        txt_content.Text = editeur.Editeur.load(txt_name.Text)
    End Sub

    Private Sub btn_save_Click(sender As Object, e As EventArgs) Handles btn_save.Click
        editeur.Editeur.save(txt_name.Text, txt_content.Text)
    End Sub
End Class
